import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import dateFormat from 'dateformat';

const CostsList = (props) => {
    const navigate = useNavigate();
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)

    const handleRemoveClick = (id,e) => {
        const apiUrl = 'https://localhost:7199/cost/delete?Id=' + id;
        fetch(apiUrl, { method: "DELETE" })
        .then((response, reject)=> {
            return fetchTableData();
        })
    }

    const handleUpdateClick = (cost, e) => {

        console.log('props');
        console.log(cost);

        navigate('/cost/form', { state: { apiUrl:"https://localhost:7199/cost/update", cost: cost } })
    }

    const fetchTableData = (t, d) => {
        const apiUrl = 'https://localhost:7199/cost/all';
        const token = localStorage.getItem("token");
        const defaultOptions = {
            headers: {
                'accept': '*/*',
                'Authorization': 'Bearer ' + token,
            },
          };
        fetch(apiUrl, defaultOptions)
        .then((response) => {
            if (!response.ok) {
                props.handleOpen(response.statusText);
            }

            return response.json()
        })
        .then((costData) => setData(costData))
        .finally(() => {
            setLoading(false)
        })
    }

    useEffect(() => {
        setLoading(true)
        fetchTableData();
    }, [])

    return loading ? (
        <div>Loading...</div>
      ) :  (<div id="costs-list">
        <Link to="/cost/form" className="add-button">Добавить расход</Link><br /><br />
        <table className="table">
            <thead>
                <tr>
                    <th>Категория</th>
                    <th>Сумма</th>
                    <th>Дата</th>
                    <th>Тег</th>
                    <th>Описание</th>
                    <th>Удалить</th>
                    <th>Редактировать</th>
                </tr>
            </thead>
            <tbody>
                {data.map((val) => {
                    return (
                        <tr key={val.id}>
                            <td>{
                                (() => {
                                    if (val.costCategory === 0) {
                                        return (<p>Налоги</p>)
                                    }
                                    else if (val.costCategory === 1) {
                                        return (<p>Аренда</p>)
                                    }
                                    else if (val.costCategory === 2) {
                                        return (<p>Авто</p>)
                                    }
                                    else if (val.costCategory === 3) {
                                        return (<p>Проезд</p>)
                                    }
                                    else if (val.costCategory === 4) {
                                        return (<p>Путешествия</p>)
                                    }
                                    else if (val.costCategory === 5) {
                                        return (<p>Техника</p>)
                                    }
                                    else if (val.costCategory === 6) {
                                        return (<p>Бытовая техника</p>)
                                    }
                                    else if (val.costCategory === 7) {
                                        return (<p>Еда</p>)
                                    }
                                    else if (val.costCategory === 8) {
                                        return (<p>Дом</p>)
                                    }
                                    else if (val.costCategory === 9) {
                                        return (<p>Ремонт</p>)
                                    }
                                    else if (val.costCategory === 10) {
                                        return (<p>Одежда</p>)
                                    }
                                    else if (val.costCategory === 11) {
                                        return (<p>Коммунальные услуги</p>)
                                    }
                                    else if (val.costCategory === 12) {
                                        return (<p>Здоровье</p>)
                                    }
                                    else if (val.costCategory === 13) {
                                        return (<p>Подписки</p>)
                                    }
                                    else if (val.costCategory === 14) {
                                        return (<p>Развлечения</p>)
                                    }
                                    else if (val.costCategory === 15) {
                                        return (<p>Непредвиденные</p>)
                                    }
                                    else if (val.costCategory === 16) {
                                        return (<p>Другое</p>)
                                    }
                                })()  
                            }</td>
                            <td>{val.sum}</td>
                            <td>{dateFormat(val.date, "dd.mm.yyyy")}</td>
                            <td>{val.tag}</td>
                            <td>{val.description}</td>
                            <td><button className="delete-btn" onClick={e => handleRemoveClick(val.id,e)}>Delete</button></td>
                            <td><button className="update-btn" onClick={e => handleUpdateClick(val,e)}>Update</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>)
}

export default CostsList;