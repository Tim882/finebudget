import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useLocation } from "react-router-dom";

const CostDetailsForm = (props) => {
  const location = useLocation();
  var stateData = location.state;

  if (stateData == null) stateData = props;

  const navigate = useNavigate();
  const [cost, setCost] = useState({
    id: stateData.cost ? stateData.cost.id : '',
    costCategory: stateData.cost ? stateData.cost.costCategory : '0',
    accountId: stateData.cost ? stateData.cost.accountId : '',
    sum: stateData.cost ? stateData.cost.sum : '0',
    date: stateData.cost ? stateData.cost.costDate : '',
    tag: stateData.cost ? stateData.cost.tag : 'sulik',
    description: stateData.cost ? stateData.cost.description : 'desc'
  });

  const [errorMsg, setErrorMsg] = useState('');
  const { id, costCategory, accountId, sum, date, tag, description} = cost;

  const handleOnSubmit = (event) => {

    event.preventDefault();

    const cost = {
      id: parseInt(id), CostCategory: parseInt(costCategory), AccountId: parseInt(accountId), Sum: parseFloat(sum), Date: new Date(), Tag: tag, Description: description
    };

    console.log('handle submin');
    console.log(cost);

    var requestType = 'POST';

    if (stateData.cost.id != '0') requestType = 'PUT';

    const token = localStorage.getItem("token");
        const defaultOptions = {
            headers: {
                'accept': '*/*',
                'Authorization': 'Bearer ' + token,
            },
        };

    const requestOptions = {
      method: requestType,
      headers: {
        'accept': '*/*',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json' 
      },
      body: JSON.stringify(cost)
    };

    const apiUrl = stateData.apiUrl;

    fetch(apiUrl, requestOptions)
    .then((response, reject)=> {
      if (!response.ok) {
        props.handleOpen(response.statusText);
      }

      navigate('/costs');
    })
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      // case 'quantity':
      //   if (value === '' || parseInt(value) === +value) {
      //     setCost((prevState) => ({
      //       ...prevState,
      //       [name]: value
      //     }));
      //   }
      //   break;
      case 'Sum':
        if (value === '' || value.match(/^\d{1,}(\.\d{0,2})?$/)) {
          setCost((prevState) => ({
            ...prevState,
            [name]: value
          }));
        }
        break;
      default:
        setCost((prevState) => ({
          ...prevState,
          [name]: value
        }));
    }
  };

  return (
    <div className="main-form" align="center">
      {errorMsg && <p className="errorMsg">{errorMsg}</p>}
      <Form onSubmit={handleOnSubmit}>
        <Form.Group controlId="id">
          <Form.Label>cost Name</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="id"
            value={id}
            placeholder="Enter id of cost"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="costCategory">
          <Form.Label>Категория</Form.Label>
          <Form.Control
          as="select"
          name="costCategory"
          value={costCategory}
          onChange={handleInputChange}
          >
            <option value="0">Налоги</option>
            <option value="1">Аренда</option>
            <option value="2">Авто</option>
            <option value="3">Проезд</option>
            <option value="4">Путешествия</option>
            <option value="5">Техника</option>
            <option value="6">Бытовая техника</option>
            <option value="7">Еда</option>
            <option value="8">Дом</option>
            <option value="9">Ремонт</option>
            <option value="10">Одежда</option>
            <option value="11">Коммунальные услуги</option>
            <option value="12">Здоровье</option>
            <option value="13">Подписки</option>
            <option value="14">Развлечения</option>
            <option value="15">Непредвиденные</option>
            <option value="16">Другое</option>
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="accountId">
          <Form.Label>Имя счета</Form.Label>
          <Form.Control
            className="input-control"
            type="number"
            name="accountId"
            value={accountId}
            placeholder="Enter available accountId"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="sum">
          <Form.Label>Сумма</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="sum"
            value={sum}
            placeholder="Enter sum of cost"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="date">
          <Form.Label>Дата</Form.Label>
          <Form.Control
            className="input-control"
            type="date"
            name="date"
            value={date}
            placeholder="Enter date of cost"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="tag">
          <Form.Label>Тег</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="tag"
            value={tag}
            placeholder="Enter tag of cost"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="description">
          <Form.Label>Описание</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="description"
            value={description}
            placeholder="Enter description of cost"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Button variant="primary" type="submit" className="add-button">
          Сохранить
        </Button>
      </Form>
    </div>
  );
};

export default function CostForm(props) {
    return <div>
      <h2>Форма добавления расхода</h2>
      <CostDetailsForm apiUrl={props.apiUrl} cost={props.cost}/>
    </div>
}

CostForm.defaultProps = {apiUrl: 'https://localhost:7199/cost/create', cost: {
  id: '0',
  costCategory: '0',
  accountId: '0',
  sum: '0',
  date: '',
  tag: '',
  description: ''
}}